import json
import sys

if __name__ == "__main__":
    for fname in sys.argv[1:]:
        with open(fname, 'rt') as f:
            for line in f:
                print(json.loads(line)["reviewText"])
