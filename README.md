# About

This directory contains jupyter notebook files that were used for testing the
dissim algorithm. The following jupyter notebook files are present:

   - `*_bare.ipynb` -- A Doc2Vec model is trained solely on the training data
     After that, the vectors for the paragraph-sentence pairs from the test set
     are inferred and each pair of vectors is scored for similarity based on
     cosine distance. The results are compared to the gold score.

   - `*_cluster-*.ipynb` -- A Doc2Vec model is trained on the lemmatized 500
     paragraph-sentence pairs from the SemEval 2014 Task 3 training set. After
     that for each word w, the documents in which w appears are clustered into
     k clusters, where k is the number of w's synsets based on WordNet. Each
     occurance of a word in a document d is labeled with the index of the
     cluster closest to d.
   
     A new Doc2Vec model is trained on the disambiguated lemmatized 500
     paragraph-sentence pairs from the SemEval 2014 Task 3 training set. After
     that, the vectors for the paragraph-sentence pairs from the test set are
     inferred and each pair of vectors is scored for similarity based on cosine
     distance. The results are compared to the gold score.
     
     - `*_cluster.ipynb` -- The k-means algorithm was used for clustering.

     - `*_cluster-spherical.ipynb` -- The spherical k-means algorithm was used
       for clustering.

     - `*_cluster-hierarchical.ipynb` -- The average-linkage k-means algorithm
       was used for clustering.

   - `*_pagerank.ipynb` -- A Doc2Vec model is trained on the lemmatized 500
     paragraph-sentence pairs from the SemEval 2014 Task 3 training set. After
     that for each word w, the documents in which w appears are clustered into
     k clusters, where k is the number of w's synsets based on WordNet. For each
     cluster, a graph is created where the vertices correspond to the documents
     in which w appears. An (u, v) exists in the graph if v is located between
     u and the cluster centroid c; the weight of (u, v) is then the cosine
     distance between c-u and c-v. The PageRank algorithm is ran on the graph. Each
     occurance of a word in a document d is labeled with the index of the
     cluster whose PageRank run produced the greatest rank for d.
   
     A new Doc2Vec model is trained on the disambiguated lemmatized 500
     paragraph-sentence pairs from the SemEval 2014 Task 3 training set. After
     that, the vectors for the paragraph-sentence pairs from the test set are
     inferred and each pair of vectors is scored for similarity based on cosine
     distance. The results are compared to the gold score.

   - `untrained_*.ipynb` -- Trained on:
     -     500 paragraph-sentence pairs from the SemEval 2014 Task 3 training set,
     totalling 1 000 training documents.

   - `trained_*.ipynb` -- Trained on:
     -     500 paragraph-sentence pairs from the SemEval 2014 Task 3 training set,
     -     307 paragraphs of Aesop's Fables,
     -   8 711 paragraphs from BBC news and sport news,
     -   4 944 book reviews from <amazon.com>, and
     -   5 235 questions and answers from Yahoo! Answers.
     totalling 20 197 training documents.

   - `well-trained_*.ipynb` -- Trained on:
     -     500 paragraph-sentence pairs from the SemEval 2014 Task 3 training set,
     -     307 paragraphs of Aesop's Fables,
     -  14 045 paragraphs from BBC news and sport news,
     -   4 944 book reviews from <amazon.com>, and
     - 157 033 questions and answers from Yahoo! Answers.
     totalling 177 329 training documents.

   - `best-trained_*.ipynb` -- Trained on:
     -        500 paragraph-sentence pairs from the SemEval 2014 Task 3 training set,
     -        307 paragraphs of Aesop's Fables,
     -     14 045 paragraphs from BBC news and sport news,
     -    157 033 questions and answers from Yahoo! Answers.
     - 23 164 469 paragraphs of text fromi English Wikipedia.
     totalling 23 336 854 training documents.

# Installation

 - Create a Python 3 virtualenv and switch to it via ``mkvirtualenv -p `which
   python3` my_virtualenv``.
 - Install the required packages via `pip install -r requirements.txt
   --no-binary :all:`.
 - Install a Python 3 kernel to `jupyter-notebook` via `ipython3 kernel install
   --user`.
 - Enter each subdirectory and use the Makefiles to retrieve and process the
   datasets. This step requires 25 GiB of free space at minimum.
 - If you intend to rerun all computations, remove any intermediary files by
   running `rm -f *.index *.doc2vec* *.epoch`.
 - Run `jupyter-notebook`.
